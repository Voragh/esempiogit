import wordcloud
import re 

def generate_from_frequencies(frequencies):
    cloud = wordcloud.WordCloud()
    cloud.generate_from_frequencies(frequencies)
    cloud.to_file("myfile.jpg")


def text_clean(text, noGood):
    new_text = []
    words = text.split()
    for word in words:
        isWord = re.match("^[a-zÀ-ÿ]+$", word.lower())
        if(isWord and word not in noGood):
            new_text.append(word)
    return " ".join(new_text)
    
def dictionary_frequency(text):
    dictionar = {}
    words = text.split()
    for word in words:
        if(word in dictionar):
            dictionar[word] += 1
        else:
            dictionar[word] = 1
    return dictionar


noGood = ("ciao")
f = open("testo.txt", "r")

text = text_clean(f.read(), noGood)
dictionary = dictionary_frequency(text)
print(dictionary)
generate_from_frequencies(dictionary)





def calculate_frequencies(file_contents):
    # Here is a list of punctuations and uninteresting words you can use to process your text
    punctuations = '''!()-[]{};:'"\,<>./?@#$%^&*_~'''
    uninteresting_words = ["the", "a", "to", "if", "is", "it", "of", "and", "or", "an", "as", "i", "me", "my", \
    "we", "our", "ours", "you", "your", "yours", "he", "she", "him", "his", "her", "hers", "its", "they", "them", \
    "their", "what", "which", "who", "whom", "this", "that", "am", "are", "was", "were", "be", "been", "being", \
    "have", "has", "had", "do", "does", "did", "but", "at", "by", "with", "from", "here", "when", "where", "how", \
    "all", "any", "both", "each", "few", "more", "some", "such", "no", "nor", "too", "very", "can", "will", "just"]
    
    # LEARNER CODE START HERE
    text = text_clean(file_contents, uninteresting_words)

    frequencies = dictionary_frequency(text)

    #wordcloud
    cloud = wordcloud.WordCloud()
    cloud.generate_from_frequencies(frequencies)
    return cloud.to_array()