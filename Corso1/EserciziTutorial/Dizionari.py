x = {}
type(x)

file_counts = {"jpg":10,"txt":14, "csv":2, "py":23}
print(file_counts)
file_counts["txt"]
"jpg" in file_counts
"html" in file_counts
file_counts["cfg"] = 8
file_counts["csv"] = 17
del file_counts["cfg"]
print(file_counts)


toc = {"Introduction":1, "Chapter 1":4, "Chapter 2":11, "Chapter 3":25, "Chapter 4":30}
toc["Epilogue"] = 39 # Epilogue starts on page 39
toc["Chapter 3"] = 24 # Chapter 3 now starts on page 24
print(toc) # What are the current contents of the dictionary?
print("Chapter 5" in toc) # Is there a Chapter 5?


for extension in file_counts:
    print(extension)

for ext, amount in file_counts.items():
    print(f"Ci sono {amount} file di tipo {ext}")

file_counts.keys()
file_counts.values()


for value in file_counts.values():
    print(value)

cool_beasts = {"octopuses":"tentacles", "dolphins":"fins", "rhinos":"horns"}
for key, value in cool_beasts.items():
    print("{key} have {value}".format(key=key, value=value))


def count_letters(text):
    result = {}
    for letter in text:
        if letter not in result:
            result[letter] = 0
        else:
            result[letter] += 1
    return result


wardrobe = {"shirt":["red","blue","white"], "jeans":["blue","black"]}
for key, value in wardrobe.items():
	for color in value:
		print("{} {}".format(color, key))