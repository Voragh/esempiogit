#print(dir(""))
#print(help(""))

class Apple:
    pass

class Apple:
    color = ""
    flavor = ""

jonagold = Apple()
jonagold.color = "red"
jonagold.flavor = "sweet"

print(jonagold.color)
print(jonagold.flavor)

golden = Apple()
golden.color = "yellow"
golden.flavor = "soft"


class Flower:
  color = 'unknown'

rose = Flower()
rose.color = "red"

violet = Flower()
violet.color = "blue"

this_pun_is_for_you = "Sugar is sweet, Are so are you."

print("Roses are {},".format(rose.color))
print("violets are {},".format(violet.color))
print(this_pun_is_for_you) 


class Piglet:
  name = "piglet"
  def speak(self):
    print(f"Oink! Io sono {self.name}! Oink!")

hamlet = Piglet()
hamlet.name = "Hamlet"
hamlet.speak()

petunia = Piglet()
petunia.name = "Petunia"
petunia.speak()

class Piglet:
  years = 0
  def pig_years(self):
    return self.years*18


class Dog:
  years = 0
  def dog_years(self):
    return self.years*7
    
fido=Dog()
fido.years=3
print(fido.dog_years())


class Apple:
  def __init__(self, color, flavor):
      self.color = color
      self.flavor = flavor
  def __str__(self) -> str:
      return f"Questa mela è {self.color} e il suo sapore è {self.flavor}"

jonagold = Apple("red", "sweet")
print(jonagold.color)
print(jonagold)



class Person:
    def __init__(self, name):
        self.name = name
    def greeting(self):
        # Should return "hi, my name is " followed by the name of the Person.
        return f"hi, my name is {self.name}" 

# Create a new instance with a name of your choice
some_person = Person("Marco")  
# Call the greeting method
print(some_person.greeting())


def to_seconds(hours, minutes, seconds):
  """ Ritorna l'ammontare di secondi 
  (Questo è un commento docstring ossia un commento che appare nella documentazione)"""
  return hours*3600 + minutes*60 + seconds

help(to_seconds)



class Person:
  def __init__(self, name):
    self.name = name
  def greeting(self):
    """ Outputs a message with the name of the person """
    print("Hello! My name is {name}.".format(name=self.name)) 

#help(Person)



class Elevator:
    def __init__(self, bottom, top, current):
        """Initializes the Elevator instance."""
        self.bottom = bottom
        self.top = top
        self.current = current
    def up(self):
        """Makes the elevator go up one floor."""
        if(self.current < self.top):
            self.current += 1
    def down(self):
        """Makes the elevator go down one floor."""
        if(self.current > self.bottom):
            self.current -= 1
    def go_to(self, floor):
        """Makes the elevator go to the specific floor."""
        if(floor >= self.bottom and floor <= self.top):
            self.current = floor
    def __str__(self) -> str:
        return(f"Current floor: {self.current}")

elevator = Elevator(-1, 10, 0)


elevator.go_to(5)
print(elevator)


class Fruit:
  def __init__(self, color, flavor):
      self.color = color
      self.flavor = flavor

class Apple(Fruit):
  pass

class Grape(Fruit):
  pass

granny_smith = Apple("Green", "Tart")
carnelian = Grape("Purple", "Sweet")

class Animal:
  sound =""
  def __init__(self, name):
      self.name = name
  def speak(self):
    print(f"{self.sound} io sono {self.name} {self.sound}")

class Piglet(Animal):
  sound = "Oink!"

hamlet = Piglet("Hamlet")
hamlet.speak()

class Cow(Animal):
  sound = "Moooo"

milky = Cow("Milky White")
milky.speak()



class Clothing:
  material = ""
  def __init__(self,name):
    self.name = name
  def checkmaterial(self):
	  print("This {} is made of {}".format(self.name,self.material))
			
class Shirt(Clothing):
  material="Cotton"

polo = Shirt("Polo")
polo.checkmaterial()



class Repository:
  def __init__(self):
      self.package = {}
  def add_package(self, package):
    self.package[package.name] = package
  def total_size(self):
    result = 0
    for package in self.package.values():
      result += package.size
    return result




class Clothing:
  stock={ 'name': [],'material' :[], 'amount':[]}
  def __init__(self,name):
    material = ""
    self.name = name
  def add_item(self, name, material, amount):
    Clothing.stock['name'].append(self.name)
    Clothing.stock['material'].append(self.material)
    Clothing.stock['amount'].append(amount)
  def Stock_by_Material(self, material):
    count=0
    n=0
    for item in Clothing.stock['material']:
      if item == material:
        count += Clothing.stock['amount'][n]
        n+=1
    return count

class shirt(Clothing):
  material="Cotton"
class pants(Clothing):
  material="Cotton"
  
polo = shirt("Polo")
sweatpants = pants("Sweatpants")
polo.add_item(polo.name, polo.material, 4)
sweatpants.add_item(sweatpants.name, sweatpants.material, 6)
current_stock = polo.Stock_by_Material("Cotton")
print(current_stock)