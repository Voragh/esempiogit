name = "Marco"
color = 'Giallo'
pet = ""
pet = "loooooooooooooooooooooooooog cat"
print("Name: " + name + " , my favorite color: " + color)
print("exemple" *3)
len(pet)

###

def double_word(word):
    stringa = word * 2
    lunghezza = len(stringa)
    return stringa + str(lunghezza)

print(double_word("hello")) # Should return hellohello10
print(double_word("abc"))   # Should return abcabc6
print(double_word(""))      # Should return 0

print(color[1])
print(color[-1])
print(color[-2])

###

def first_and_last(message):
    if(len(message) > 0):
        return message[0] == message[-1]
    return True

print(first_and_last("else"))
print(first_and_last("tree"))
print(first_and_last(""))
print(color[1:4])
fruit = "pineapple"
print(fruit[:4])
print(fruit[4:])


message = "A kong string with a silly typo"
new_message = message[:2] + "l" + message[3:]
print(new_message)

pets = "Cats & Dogs"
pets.index("&")
pets.index("Dog")
pets.index("s")

"Dragons" in pets
print("Cats" in pets)
print("Cat" in pets)

###

def replace_domain(email, old_domain, new_domain):
    if("@" + old_domain in email):
        index = email.index("@"+old_domain)
        new_email = email[:index] + "@" + new_domain
        return new_email
    return email

###

color.upper()
color.lower()
print(" ciao ".strip())
print(" ciao ".lstrip())
print(" ciao ".rstrip())
print(color.count("l"))
print("endswith",color.endswith("llo"))
print("123123123".isnumeric())
print(type(int("1254254")))
print("...".join(["Questa", "è", "una","frase"]))
"Un altra frase".split()



###

def initials(phrase):
    words = phrase.upper().split()
    result = ""
    for word in words:
        result += word[0]
    return result

print(initials("Universal Serial Bus")) # Should be: USB
print(initials("local area network")) # Should be: LAN
print(initials("Operating system")) # Should be: OS



###

name = "Manny"
number = len(name)*3
print("Hello {} il tuo numero fortunato è {}".format(name, number))
print("il tuo numero fortunato è {number} e ti chiami {name}".format(name=name, number=number))


###

def student_grade(name, grade):
	return "{name} received {grade}% on the exam".format(name=name, grade=grade)

print(student_grade("Reed", 80))
print(student_grade("Paige", 92))
print(student_grade("Jesse", 85))

###

price = 7.5
with_tax = price *1.09
print(price, with_tax)
print("Prezzo base: {price:.2f}. Whit tax {with_tax:.2f}".format(price=price, with_tax=with_tax))


def to_celsius(x):
    return (x-32)*5/9

for x in range(0,101,10):
    #print(x, to_celsius(x))
    print("{:3} F | {:>6.2f} C".format(x, to_celsius(x)))