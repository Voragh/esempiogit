print(10>1)
print("cat" == "dog")
print(1 != 2)
#ERRORE print(1 < "1")
print(1 == "1")
print("cat" > "Cat")

print("Yellow" > "Cyan" and "Brown" > "Magenta")
print(25 > 50 or 1 != 2)
print(not 42 == "Answer")

def hint_username(username):
    if len(username)<3:
        print("Username Invalido, devono essere presenti almeno 3 caratteri")
    else:
        if len(username) > 15:
            print("Username Invalido, devono essere presenti meno di 15 caratteri")
        else:
            print("Username Valido")

def hint_username_elif(username):
    if len(username)<3:
        print("Username Invalido, devono essere presenti almeno 3 caratteri")
    elif len(username) > 15:
        print("Username Invalido, devono essere presenti meno di 15 caratteri")
    else:
        print("Username Valido")


def is_positive(number):
  if number > 0:
    return True
  else:
    return False

def is_even(number):
    if number % 2 == 0:
        return True
    return False


def number_group(number):
  if number > 0:
    return "Positive"
  elif number < 0:
    return "Negative"
  else:
    return "Zero"

print(number_group(10)) #Should be Positive
print(number_group(0)) #Should be Zero
print(number_group(-5)) #Should be Negative