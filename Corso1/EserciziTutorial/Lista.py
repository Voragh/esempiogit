x = ["Now","we","are","cooking"]
type(x)
len(x)
"are" in x
"Today" in x
print ("cook" in x)
x[-1]
x[1]
print(x[1:3])
x[2:]
x[:2]


def get_word(sentence, n):
	# Only proceed if n is positive 
	if n > 0:
		words = sentence.split()
		# Only proceed if n is not more than the number of words 
		if n <= len(words):
			return(words[n-1])
	return("")

print(get_word("This is a lesson about lists", 4)) # Should print: lesson
print(get_word("This is a lesson about lists", -4)) # Nothing
print(get_word("Now we are cooking!", 1)) # Should print: Now
print(get_word("Now we are cooking!", 5)) # Nothing


fruits = ["Pineapple", "Banana", "Apple", "Melon"]
fruits.append("Kiwi")
fruits.insert(0, "Orange")
fruits.insert(25, "Peach")
fruits.remove("Melon")
print(fruits.pop(3))
fruits[2] = "Strawberry"
print(fruits)




def skip_elements(elements):
	# Initialize variables
	new_list = []
	i = 0

	# Iterate through the list
	for elem in elements:
		# Does this element belong in the resulting list?
		if i % 2 == 0:
			# Add this element to the resulting list
			new_list.append(elem)
		# Increment i
		i += 1

	return new_list

print(skip_elements(["a", "b", "c", "d", "e", "f", "g"])) # Should be ['a', 'c', 'e', 'g']
print(skip_elements(['Orange', 'Pineapple', 'Strawberry', 'Kiwi', 'Peach'])) # Should be ['Orange', 'Strawberry', 'Peach']
print(skip_elements([])) # Should be []



animals = ["Lion", "Zebra", "Dolphin", "Monkey"]
chars = 0
for animal in animals:
    chars += len(animal)
print(f"Totale della lunghezza {chars}. Media della lunghezza {chars/len(animals)}")

winners = ["Marco", "Paolo", "Andrea"]
for index, person in enumerate(winners):
    print(f"{index+1} - {person}")


def skip_elements(elements):
	# code goes here
    new_list = []
    for index, elem in enumerate(elements):
        if(index %2 == 0):
            new_list.append(elem)

    return new_list

print(skip_elements(["a", "b", "c", "d", "e", "f", "g"])) # Should be ['a', 'c', 'e', 'g']
print(skip_elements(['Orange', 'Pineapple', 'Strawberry', 'Kiwi', 'Peach'])) # Should be ['Orange', 'Strawberry', 'Peach']


def full_emails(people):
    result = []
    for email, name in people:
        result.append(f"{name} <{email}>")
    return result

print(full_emails([("marco@example.com", "Marco Monti"),("paolo@example.com", "Paolo Mele"),("andrea@example.com", "Andrea Villani")]))


multiples = []
for x in range(1,11):
    multiples.append(x*7)

multiples = [x * 7 for x in range(1,11)]

languages = ["Python", "Perl", "Ruby", "Go", "Java", "C"]
lengths = [len(language) for language in languages]

z = [x for x in  range(0,101) if x % 3 == 0]


def odd_numbers(n):
	return [x for x in range(0,n+1) if x % 2 != 0]

print(odd_numbers(5))  # Should print [1, 3, 5]
print(odd_numbers(10)) # Should print [1, 3, 5, 7, 9]
print(odd_numbers(11)) # Should print [1, 3, 5, 7, 9, 11]
print(odd_numbers(1))  # Should print [1]
print(odd_numbers(-1)) # Should print []