for left in range(7):
    for right in range(left,7):
        print("[", left, "|", right, "]", end=" ")
    print()

###

teams = ["Dreagons", "Wolves", "Pandas", "Unicorns"]
for home_team in teams:
    for away_team in teams:
        if(home_team != away_team):
            print(home_team, "vs", away_team)

###

def greet_friends(friends):
    for friend in friends:
        print("Hi", friend)


friends = ['Taylor', 'Alex', 'Pat', 'Eli']
greet_friends(friends)
greet_friends("Barry")


###

def is_valid(user):
    if(len(user) > 3):
        return True
    return False

def validate_users(users):
  for user in users:
    if is_valid(user):
      print(user + " is valid")
    else:
      print(user + " is invalid")

validate_users(["purplecat"])