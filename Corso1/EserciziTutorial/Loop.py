x = 0 
while x < 5:
    print("Non Ancora x = " + str(x))
    x = x+1
print("x = " + str(x))


####
def attempts(n):
    x = 1
    while x <= n:
        print("Attempt " + str(x))
        x += 1
    print("Done")
    
attempts(5)


###

#username = get_username()
#while not valid_username(username):
#    print("Invalid Username")
#    username = get_username()

my_variable = 5
while my_variable < 10:
    print("Hello")
    my_variable += 1

x = 1
sum = 0 
while x < 10:
    sum += x
    x += 1

product = 1
while x < 10:
    product *= x
    x += 1

####

def count_down(start_number):
    current = start_number
    while (current > 0):
        print(current)
        current -= 1
    print("Zero!")

count_down(3)

######
if x != 0:
    while x % 2 == 0:
        x = x / 2


while x % 2 == 0 and x != 0:
    x = x / 2

###

def print_range(start, end):
	# Loop through the numbers from start to end
    n = start
    while n <= end:
        print(n)
        n += 1

print_range(1, 5)  # Should print 1 2 3 4 5 (each number on its own line) 

####
#while True:
#    do_something_cool()
#    if user_requested_to_stop():
#        break