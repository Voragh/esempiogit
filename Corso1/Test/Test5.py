def factorial(n):
    result = 1
    for x in range(1,n+1):
        result = x * result
    return result

for n in range(0,10):
    print(n, factorial(n))


###

for x in range(1,11):
  print(x**3)

###

for elem in range(0,100,7):
    print(elem)

###

def retry(operation, attempts):
  for n in range(attempts):
    if operation():
      print("Attempt " + str(n) + " succeeded")
      break
    else:
      print("Attempt " + str(n) + " failed")

#retry(create_user, 3)
#retry(stop_service, 5)