print("Automating with Python is fun!")

color = "Yellow"
thing = "sunshine"
print(color + " is the color of " + thing)

secondForDay = 86400
week = 7*secondForDay
print(week)

possibilityForLetter = 26
totalPossibility = 1
for i in range(6):
    totalPossibility *= possibilityForLetter
print(totalPossibility)

disk_size = 16*1024*1024*1024
sector_size = 512
sector_amount = disk_size / sector_size

print(sector_amount)