#!/usr/bin/env python3

import sys
import subprocess

if(len(sys.argv) >1):
    file = sys.argv[1]
else:
    file = "Corso2/bash/oldFiles.txt"


    with open(file) as lines:
        for line in lines.readlines():
            line = line.strip()
            new_line =  line.replace("jane", "jdoe")
            command = "mv ..{line} ..{new_line}".format(line=line, new_line=new_line)
            print(command)
            #subprocess.run([command], shell = True)