#!/bin/bash

# leggi 
# | trasforma ' ' in '\n' 
# | ordina in ordine alfabetico 
# | verifica quante ripetizioni ci sono e le conta 
# | passa il comando al sort e le ordina i numeri inversamente
# | stampa le prime 10 righe
#cat spider.txt | tr ' ' '\n' | sort | uniq -c | sort -nr | head

line="-------------------";

echo "Starting at: $(date)";
echo $line;

echo "UPTIME";
uptime;
echo $line;

echo "FREE";
free;
echo $line;

echo "WHO";
who;
echo $line;

echo "Finishing at: $(date)";




if grep "127.0.0.1" /etc/hosts; then
    echo "Everything ok";
else
    echo "Error! 127.0.0.1 is not in /etc/hosts";
fi

if test -n "$PATH"; then
    echo "Your path is not empty";
fi

if [ -n "$PATH" ]; then
    echo "Your path is not empty";
fi


#n=1
#while [ $n -le 5 ]; do
#    echo "Iterazione numero $n"
#    ((n+=1))
#done


#n=0
#command=$1
#while ! $command && [ $n -le 5 ]; do
#    echo "Iterazione numero $n"
#    ((n+=1))
#    echo "Retry #$n"
#done

for fruit in peach orange apple; do
    echo "I like $fruit"
done


#for file in *.HTM; do
#    name=$(basename "$file" .HTM);
#    echo mv "$file" "$name.html"
#done

for logfile in /var/log/*log; do
    echo "Processing: $logfile"
    cut -d' ' -f5- $logfile | sort | uniq -c | sort -nr | head
done