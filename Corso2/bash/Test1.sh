#!/bin/bash

> oldFiles.txt;
files=$(grep " jane " list.txt | cut -d' ' -f 3)

for file in $files; do
    if [ -e "..$file" ]; then
        echo "$file" >> oldFiles.txt

    else
        echo "Non Trovato: $file"
    fi
done
