#from Regrex import rearrange_name
#
#print(rearrange_name("Marco, Monti"))


import re
def rearrange_name(name):
  result = re.search(r"^([\w \.-]*), ([\w \.-]*)$", name)
  if result == None:
    return name
  return "{} {}".format(result[2], result[1])

print(rearrange_name("Monti, Marco"))



def character_frequency(filename):
  try:
    f = open(filename)
  except OSError:
    return None
  characters = {}
  for line in f:
    for char in line:
      characters[char]=characters.get(char, 0) +1
  f.close()
  return characters



def validate_user(username,minlen):
  assert type(username) == str, "username must be string"
  if minlen < 1:
    raise ValueError("minlen must be at least 1")
  if(len(username) < minlen):
    return False
  if not username.isalnum():
    return False
  
  return True


print(validate_user("Marco", 1))