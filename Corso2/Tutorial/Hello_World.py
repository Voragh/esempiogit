#!/usr/bin/env python3
print("Hello World")


import Areas

print(Areas.triangle(3,5))
print(Areas.circle(4))

import shutil

du = shutil.disk_usage("/")
print(du)

print(du.free / du.total * 100)

import psutil

print(psutil.cpu_percent(0.1), "%")
print(psutil.cpu_percent(0.5), "%")