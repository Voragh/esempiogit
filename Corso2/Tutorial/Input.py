#name = input("Inserire il proprio nome: ")
#print("Hello", name)
#
#def to_seconds(hours, minutes, seconds):
#    return hours*3600+minutes*60+seconds
#
#print("Welcome to this time converter")
#
#cont = "y"
#
#while(cont.lower() == "y"):
#    hours = int(input("Inserire il numero di ore: "))
#    minutes = int(input("Inserire il numero di minuti: "))
#    seconds = int(input("Inserire il numero di secondi: "))
#
#    print(f"Totale secondi {to_seconds(hours, minutes,seconds)}")
#    print()
#    cont = input("Vuoi inserire una nuova conversione?")
#
#print("Ciao Ciao")


import os

print("Home: "+ os.environ.get("HOME",""))
print("Shell: "+ os.environ.get("SHELL",""))
print("Go Path: "+ os.environ.get("GOPATH",""))

import sys

#Si possono visualizzare le variabili lanciate con il comando sys.argv
#py .\Corso2\Tutorial\Input.py ciao bello
print(sys.argv)


if(len(sys.argv)>1):
    filename= sys.argv[1]

    if(not os.path.exists(filename)):
        with open(filename, "w") as f:
            f.write("Creato un nuovo file")
    else:
        print(f"Errore il file {filename} già esiste")
        sys.exit(1)


import subprocess

path = 'C:\Program Files\Git\\bin\\bash.exe'
subprocess.run([path, '-c','date'])
#print(subprocess.run(["powershell.exe","Get-Date"]))
#print(subprocess.run([path, "-c","sleep 2"]))


result = subprocess.run([path, "-c","host 8.8.8.8"], capture_output=True)
print(result.returncode)
print(result.stdout)
print(result.stderr)


result = subprocess.run([path, "-c","ps"], capture_output=True)
print(result.returncode)
print(result.stdout)
print(result.stdout.decode().split())


#import os
#
#my_env = os.environ.copy()
#my_env["PATH"] = os.pathsep.join(["Corso2/Tutorial/", my_env["PATH"]])
#
#result = subprocess.run(["Input.py"], env=my_env)