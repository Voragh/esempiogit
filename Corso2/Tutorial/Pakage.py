import requests

response = requests.get("http://www.google.com")
print(len(response.text))

import arrow

date = arrow.get("2020-01-18", "YYYY-MM-DD")
print(date.format())
date = date.shift(weeks=+6)
print(date.format("MMM DD YYYY"))

import PIL.Image
image = PIL.Image.open("myfile.jpg")
print(image.size)
print(image.format)


import pandas
visitors = [1235, 6424, 9345, 8464, 2345]
errors = [23, 45, 33, 45, 76]
df = pandas.DataFrame({"visitors": visitors, "errors": errors, "index": ["Mon", "Tues", "Wed", "Thu", "Fri"]})
print(df)
print(df["errors"].mean())