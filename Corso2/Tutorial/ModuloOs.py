from genericpath import isdir
import os

if os.path.exists("novel.txt"):
    os.remove("novel.txt")

if os.path.exists("first_draft.txt"):
    os.rename("first_draft.txt", "finished_masterpiece.txt")

print(os.path.getsize("spider.txt"))
print(os.path.getmtime("spider.txt"))

import datetime

timestamp = os.path.getmtime("spider.txt")
print(datetime.datetime.fromtimestamp(timestamp))


import os
file= "file.dat"
if os.path.isfile(file):
    print(os.path.isfile(file))
    print(os.path.getsize(file))
else:
    print(os.path.isfile(file))
    print("File not found")


print(os.path.abspath("spider.txt"))

print(os.getcwd())

if(not os.path.isdir("new_dir")):
    os.makedirs("new_dir")
os.chdir("new_dir")
print(os.getcwd())

if(not os.path.isdir("newer_dir")):
    os.makedirs("newer_dir")

if(os.path.isdir("newer_dir")):
    os.rmdir("newer_dir")

os.chdir("..")
print(os.getcwd())

print(os.listdir(os.getcwd()))

dir = os.getcwd()
for name in os.listdir(dir):
    fullname = os.path.join(dir, name)
    if(os.path.isdir(fullname)):
        print(f"{name} è una directory")
    else:
        print(f"{name} è un file")




