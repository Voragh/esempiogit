from unittest.case import expectedFailure
from UnitTest import validate_user
from UnitTest import rearrange_name
import unittest

class TestRearrange(unittest.TestCase):
    def test_basic(self):
        testcase = "Monti, Marco"
        expected = "Marco Monti"
        self.assertEqual(rearrange_name(testcase), expected)

    def test_empty(self):
        testcase = ""
        expected = ""
        self.assertEqual(rearrange_name(testcase), expected)

    def test_double_name(self):
        testcase = "Hopper, Grace M."
        expected = "Grace M. Hopper"
        self.assertEqual(rearrange_name(testcase), expected)

    def test_one_name(self):
        testcase = "Marco"
        expected = "Marco"
        self.assertEqual(rearrange_name(testcase), expected)


class TestValidateUser(unittest.TestCase):
    def test_valid(self):
        self.assertEqual(validate_user("validuser",3),True)
    
    def test_too_short(self):
            self.assertEqual(validate_user("inv",5),False)


    def test_invalid_characters(self):
            self.assertEqual(validate_user("invalid_user",1),False)

    def test_invalid_minlen(self):
        self.assertRaises(ValueError, validate_user, "user", -1)
    


unittest.main()