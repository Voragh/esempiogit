import re

log = "July 31 07:51:48 mycomputer bad_process[12345]: ERROR Performing package upgrade"

index = log.index("[")
print(log[index+1:index+6])

regex = r"\[(\d+)\]"
result = re.search(regex, log)
#le parentesi tonde definiscono i gruppi in questo caso il gruppo 0 è tutta l'espressione e il gruppo 1 quello dentro la parentesi tonda
print(result[1])

result = re.search(r"aza", "plaza")
print(result)

result = re.search(r"aza", "bazaar")
print(result)

result = re.search(r"aza", "maze")
print(result)

result = re.search(r"^x", "xenon")
print(result)

print(re.search(r"p.ng", "penguin"))
print(re.search(r"p.ng", "claping"))
print(re.search(r"p.ng", "sponge"))



def check_aei (text):
  result = re.search(r"a.e.i", text)
  return result != None

print(check_aei("academia")) # True
print(check_aei("aerial")) # False
print(check_aei("paramedic")) # True


print(re.search(r"p.ng", "Pangaea", re.IGNORECASE))

print(re.search(r"[pP]ython", "Python"))

print(re.search(r"[a-z]way", "The end of the hightway"))
print(re.search(r"[a-z]way", "What a way to go"))
print(re.search(r"cloud[a-zA-Z0-9]", "clouddy"))
print(re.search(r"cloud[a-zA-Z0-9]", "cloud9"))


import re
def check_punctuation (text):
  result = re.search(r"[.?!]$", text)
  return result != None

print(check_punctuation("This is a sentence that ends with a period.")) # True
print(check_punctuation("This is a sentence fragment without a period")) # False
print(check_punctuation("Aren't regular expressions awesome?")) # True
print(check_punctuation("Wow! We're really picking up some steam now!")) # True
print(check_punctuation("End of the line")) # False

print(re.search(r"[^a-zA-Z]", "This is a sentence with spaces."))
print(re.search(r"[^a-zA-Z ]", "This is a sentence with spaces."))

print(re.search(r"cat|dog", "i like cats."))

print(re.search(r"cat|dog", "i like dogs. And i loke cats"))
print(re.findall(r"cat|dog", "i like dogs. And i loke cats"))

print(re.search(r"Py.*n", "Pygmalion"))
print(re.search(r"Py.*n", "Python Programming"))

print(re.search(r"Py[a-z]*n", "Python Programming"))

print(re.search(r"Py[a-z]*n", "Pyn"))

print(re.search(r"o+l+", "goldfish"))
print(re.search(r"o+l+", "ooll"))

print(re.search(r"o+l+", "boil"))



import re
def repeating_letter_a(text):
  result = re.search(r"[aA].+[aA]", text)
  return result != None

print(repeating_letter_a("banana")) # True
print(repeating_letter_a("pineapple")) # False
print(repeating_letter_a("Animal Kingdom")) # True
print(repeating_letter_a("A is for apple")) # True



print(re.search(r"p?each", "To each their own"))
print(re.search(r"p?each", "i like peaches"))


print(re.search(r".com", "welcome"))
print(re.search(r"\.com", "welcome"))

print(re.search(r"\.com", "mydomain.com"))

print(re.search(r"\w*", "This is an example"))
print(re.search(r"\w*", "and_this_is_another"))


import re
def check_character_groups(text):
  result = re.search(r"[\w]+[\s]+[\w]+", text)
  return result != None

print(check_character_groups("One")) # False
print(check_character_groups("123  Ready Set GO")) # True
print(check_character_groups("username user_01")) # True
print(check_character_groups("shopping_list: milk, bread, eggs.")) # False

print(re.search(r"A.*a", "Argentina"))
print(re.search(r"A.*a", "Azerbaijan"))
print(re.search(r"^A.*a$", "Azerbaijan"))
print(re.search(r"^A.*a$", "Australia"))

pattern = r"^[a-zA-Z_][a-zA-Z0-9_]*$"
print(re.search(pattern, "_this_is_a_valid_variable_name"))
print(re.search(pattern, "this isn't a valid variable name"))
print(re.search(pattern, "my_variable1"))
print(re.search(pattern, "2my_variable1"))


import re
def check_sentence(text):
  result = re.search(r"^[A-Z][a-z ]*[!.?]$", text)
  return result != None

print(check_sentence("Is this is a sentence?")) # True
print(check_sentence("is this is a sentence?")) # False
print(check_sentence("Hello")) # False
print(check_sentence("1-2-3-GO!")) # False
print(check_sentence("A star is born.")) # True

print()

result = re.search(r"^(\w*), (\w*)$","Lovelace, Ada")
print(result)
print(result.groups())
print(result[0])
print(result[1])
print(result[2])
print(f"{result[2]} {result[1]}")

def rearrange_name(name):
    result = re.search(r"^(\w*), (\w*)$",name)
    if result is None:
        return name
    return f"{result[2]} {result[1]}"

print(rearrange_name("Marco, Monti"))
print(rearrange_name("Andrea, Villani"))
print(rearrange_name("Paolo Giovanni, Mele"))


import re
def rearrange_name(name):
  #result = re.search(r"^(\w*), ([A-Za-z\. ]*)$", name)
  result = re.search(r"^([\w \.-]*), ([\w \.-]*)$", name)
  if result == None:
    return name
  return "{} {}".format(result[2], result[1])

name=rearrange_name("Kennedy, John F.")
print(name)

print(re.search(r"[A-Za-z]{5}","a ghost"))

print(re.findall(r"[A-Za-z]{5}","a scary ghost appared"))

# \b sta per i limiti
print(re.findall(r"\b[A-Za-z]{5}\b","a scary ghost appared"))

print(re.findall(r"\w{5,10}","I really like strawberries"))

print(re.search(r"s\w{,20}","I really like strawberries"))


import re
def long_words(text):
  pattern = r"\b\w{7,}\b"
  result = re.findall(pattern, text)
  return result

print(long_words("I like to drink coffee in the morning.")) # ['morning']
print(long_words("I also have a taste for hot chocolate in the afternoon.")) # ['chocolate', 'afternoon']
print(long_words("I never drink tea late at night.")) # []


log = "July 31 07:51:48 mycomputer bad_process[12345]: ERROR Performing package upgrade"

regex = r"\[(\d+)\]"
result = re.search(regex, log)
print(result[1])

result = re.search(regex, "A complety different string that also has number [34567]")
print(result[1])

result = re.search(regex, "99 elephant in a [cage]")
#print(result[1])

def extract_pid(log_line):
    regex = r"\[(\d+)\]"
    result = re.search(regex, log_line)
    if(result is None):
        return ""
    return result[1]

print(extract_pid(log))
print(extract_pid("99 elephant in a [cage]"))




import re
def extract_pid(log_line):
    regex = r"\[(\d+)\]: ([A-Z]+)"
    result = re.search(regex, log_line)
    if result is None:
        return None
    return "{} ({})".format(result[1], result[2])

print(extract_pid("July 31 07:51:48 mycomputer bad_process[12345]: ERROR Performing package upgrade")) # 12345 (ERROR)
print(extract_pid("99 elephants in a [cage]")) # None
print(extract_pid("A string that also has numbers [34567] but no uppercase message")) # None
print(extract_pid("July 31 08:08:08 mycomputer new_process[67890]: RUNNING Performing backup")) # 67890 (RUNNING)


print(re.split(r"[.?!]","un rigo. uun altro rigo? l'ultimo rigo!"))

print(re.split(r"([.?!])","un rigo. uun altro rigo? l'ultimo rigo!"))


print(re.sub(r"[\w.%+-]+@[\w.-]+", "[REDACTED]", "Received an email for go_nuts95@my.example.com"))

print(re.sub(r"^([\w .-]*), ([\w .-]*)$",r"\2 \1", "Lovelace, Ada"))

print(re.split(r"the|a", "One sentence. Another one? And the last one!"))
