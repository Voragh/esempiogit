# Tutti: grep ticky syslog.log
# Errore: grep "ERROR" syslog.log
# Per Tipo: grep ERROR [messaggio] [nome file]


import re
lineInfo = "May 27 11:45:40 ubuntu.local ticky: INFO: Created ticket [#1234] (username)"
lineError = "May 27 11:45:40 ubuntu.local ticky: ERROR: Error creating ticket [#1234] (username)"
re.search(r"ticky: INFO: ([\w ]*) ", lineInfo)
re.search(r"ticky: ERROR: ([\w ]*) ", lineError)


re.search(r"ticky: INFO: ([\w ]*) .*(\(\w*\))", lineInfo)
re.search(r"ticky: INFO: ([\w ]*) .*(\("+ "ciao" + r"\))", lineInfo)


fruit = {"oranges": 3, "apples": 5, "bananas": 7, "pears": 2}
print(sorted(fruit.items()))


import operator

print(sorted(fruit.items(), key=operator.itemgetter(1),reverse=True))


#./csv_to_html.py user_emails.csv /var/www/html/<html-filename>.html

import sys

if(len(sys.argv) >2):
    file_user_emails = sys.argv[1]
    path_to_move = sys.argv[2]

else:
    file_user_emails = "user_emails2.csv"
    path_to_move = "html/<html-filename>.html"

print()

##########################################################################


import csv
import re
import operator


def collapse_log(syslog):

    #Inizializzazione
    dict_info = {}
    dict_error = {}

    regex_info = r"ticky: INFO ([\w ]*) .*\(([\w.]*)\)$"
    regex_error = r"ticky: ERROR ([\w ]*) .*\(([\w.]*)\)$"

    file_info = "user_statistics.csv"
    file_error = "error_message.csv"

    #Apro il file
    with open(syslog) as file:
        for line in file:
            line = line.strip()
            
            match_info = re.search(regex_info, line)
            match_error = re.search(regex_error, line)

            #Incremento i Dizionari in base alla regex
            if(match_info):
                dict_info[match_info.group(2)] = dict_info.get(match_info.group(2), (0,0))
                inf, err = dict_info[match_info.group(2)]
                dict_info[match_info.group(2)] = inf+1,err

            elif(match_error):
                dict_info[match_error.group(2)] = dict_info.get(match_error.group(2), (0,0))
                inf, err = dict_info[match_error.group(2)]
                dict_info[match_error.group(2)] = inf,err+1

                dict_error[match_error.group(1)] = dict_error.get(match_error.group(1), 0) +1

    #Ordinamento
    dict_info = sorted(dict_info.items())
    dict_error = sorted(dict_error.items(), key=operator.itemgetter(1), reverse=True)

    #Scrittura su file
    with open(file_info, "w", newline='', encoding='utf-8') as file:
        writer = csv.writer(file)
        writer.writerow(["Username", "INFO", "ERROR"])
        for name, (info, error) in dict_info:
            writer.writerow([name, info, error])

    with open(file_error, "w",newline='', encoding='utf-8') as file:
        writer = csv.writer(file)
        writer.writerow(["Error", "Count"])
        writer.writerows(dict_error)


    return (dict_info, dict_error)


print(collapse_log("sysolog2.log"))
##########################################################################


#./csv_to_html.py error_message.csv /var/www/html/<html-filename>.html
#./csv_to_html.py user_statistics.csv /var/www/html/<html-filename>.html

import sys
import subprocess
import os


def csv_to_html():

    if(len(sys.argv) >2):
        line = sys.argv[1]
        new_line = sys.argv[2]

    else:
        line = "error_message.csv"
        new_line = "/var/www/html/<html-filename>.html"


    path = os.path.dirname(new_line)
    name_file = os.path.basename(line).split(".")[0]

    full_path =os.path.join(path, name_file)
    full_path = full_path + ".html"

    #command = "mv {line} {full_path}".format(line=line, new_line=new_line)
    #print(command)


csv_to_html()