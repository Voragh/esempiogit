def create_python_script(filename):
  comments = "# Start of a new Python program"
  with open(filename, "w") as file:
    filesize = file.write(comments)
    return(filesize)

print(create_python_script("program.py"))



################


from genericpath import isfile
import os
from posixpath import dirname

def new_directory(directory, filename):
  # Before creating a new directory, check to see if it already exists
  if os.path.isdir(directory) == False:
    os.mkdir(directory)

  # Create the new file inside of the new directory
  os.chdir(directory)
  with open (filename, "w") as file:
    pass

  # Return the list of files in the new directory
  list = os.listdir(os.path.join(os.getcwd()))
  listFile = []
  for elem in list:
      if(os.path.isfile(elem)):
          listFile.append(elem)

  return listFile

print(new_directory("PythonPrograms", "script.py"))



#####
os.chdir("..")

import os
import datetime

def file_date(filename):
  # Create the file in the current directory
  if(not os.path.isfile(filename)):
      with open(filename, "w") as file:
        timestamp = os.path.getmtime(filename)
  else:
      os.remove(filename)
      with open(filename, "w") as file:
        timestamp = os.path.getmtime(filename)


  # Convert the timestamp into a readable format, then into a string
  tempo_di_creazione = datetime.datetime.fromtimestamp(timestamp).strftime("%Y-%m-%d")

  # Return just the date portion 
  # Hint: how many characters are in “yyyy-mm-dd”? 
  return ("{}".format(tempo_di_creazione))

print(file_date("newfile.txt")) 
# Should be today's date in the format of yyyy-mm-dd



##############


import os
def parent_directory():
  # Create a relative path to the parent 
  # of the current working directory 
  relative_parent = os.path.join(os.getcwd(), '..')
  print(relative_parent)

  # Return the absolute path of the parent directory
  return os.path.abspath(relative_parent)

print(parent_directory())